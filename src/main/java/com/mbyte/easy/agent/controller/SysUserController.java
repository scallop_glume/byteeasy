package com.mbyte.easy.agent.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.agent.entity.SysUser;
import com.mbyte.easy.agent.service.ISysUserService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@Controller
@RequestMapping("/agent/sysUser")
public class SysUserController extends BaseController  {

    private String prefix = "agent/sysUser/";

    @Autowired
    private ISysUserService sysUserService;

    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param sysUser
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String createtimeSpace, String updatetimeSpace, SysUser sysUser) {
        Page<SysUser> page = new Page<SysUser>(pageNo, pageSize);
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();
        if(!ObjectUtils.isEmpty(sysUser.getCreatetime())) {
            queryWrapper = queryWrapper.like("createtime",sysUser.getCreatetime());
         }
        if(!ObjectUtils.isEmpty(sysUser.getPassword())) {
            queryWrapper = queryWrapper.like("password",sysUser.getPassword());
         }
        if(!ObjectUtils.isEmpty(sysUser.getUpdatetime())) {
            queryWrapper = queryWrapper.like("updatetime",sysUser.getUpdatetime());
         }
        if(!ObjectUtils.isEmpty(sysUser.getUsername())) {
            queryWrapper = queryWrapper.like("username",sysUser.getUsername());
         }
        if(!ObjectUtils.isEmpty(sysUser.getAvailable())) {
            queryWrapper = queryWrapper.like("available",sysUser.getAvailable());
         }
        if(!ObjectUtils.isEmpty(sysUser.getEmail())) {
            queryWrapper = queryWrapper.like("email",sysUser.getEmail());
         }
        if(!ObjectUtils.isEmpty(sysUser.getTel())) {
            queryWrapper = queryWrapper.like("tel",sysUser.getTel());
         }
        if(!ObjectUtils.isEmpty(sysUser.getSexType())) {
            queryWrapper = queryWrapper.like("sex_type",sysUser.getSexType());
         }
        if(!ObjectUtils.isEmpty(sysUser.getSalary())) {
            queryWrapper = queryWrapper.like("salary",sysUser.getSalary());
         }
        if(!ObjectUtils.isEmpty(sysUser.getVipNum())) {
            queryWrapper = queryWrapper.like("vipNum",sysUser.getVipNum());
         }
        IPage<SysUser> pageInfo = sysUserService.page(page, queryWrapper);
        model.addAttribute("createtimeSpace", createtimeSpace);
        model.addAttribute("updatetimeSpace", updatetimeSpace);
        model.addAttribute("searchInfo", sysUser);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix+"list";
    }

    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }
    /**
    * 添加
    * @param sysUser
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(SysUser sysUser){
        return toAjax(sysUserService.save(sysUser));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        model.addAttribute("sysUser",sysUserService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param sysUser
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(SysUser sysUser){
        return toAjax(sysUserService.updateById(sysUser));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(sysUserService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(sysUserService.removeByIds(ids));
    }

}

