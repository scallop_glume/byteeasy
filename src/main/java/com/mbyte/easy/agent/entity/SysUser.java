package com.mbyte.easy.agent.entity;

import com.mbyte.easy.common.entity.BaseEntity;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class SysUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    private LocalDateTime createtime;

    /**
     * 密码
     */
    private String password;

    /**
     * 更新时间
     */
    private LocalDateTime updatetime;

    /**
     * 姓名
     */
    private String username;

    /**
     * 状态
     */
    private Boolean available;

    /**
     * E-mail
     */
    private String email;

    /**
     * 电话
     */
    private String tel;

    /**
     * 性别
     */
    private Integer sexType;

    /**
     * 工资
     */
    private Integer salary;

    /**
     * 发展vip数量
     */
    @TableField("vipNum")
    private Integer vipNum;


}
