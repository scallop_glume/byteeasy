package com.mbyte.easy.agent.mapper;

import com.mbyte.easy.agent.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
public interface AgentUserMapper extends BaseMapper<SysUser> {
    /**
     * 根据账号更改
     * @param agent
     * @return 受影响条数
     */
    int updateByUserAccount(SysUser agent);

    /**
     * 根据账号查找用户信息
     *
     * @author LR
     * @ 2020年2月10日16:39:48
     * @return 返回account对应的对象
     */
     SysUser findByUserAccount(String account);
}
