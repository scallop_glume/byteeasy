package com.mbyte.easy.agent.service;

import com.mbyte.easy.agent.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
public interface ISysUserService extends IService<SysUser> {
    /**
     *
     * @param agent
     * @return 受影响条数
     */
    int updateByUserAccount(SysUser agent);

    /**
     * @param account
     * @Author LR
     * @date 2020年2月10日16:50:31
     */
    SysUser findByUserAccount(String account);
}
