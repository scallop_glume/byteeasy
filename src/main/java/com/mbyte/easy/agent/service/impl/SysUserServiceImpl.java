package com.mbyte.easy.agent.service.impl;

import com.mbyte.easy.agent.entity.SysUser;
import com.mbyte.easy.agent.mapper.AgentUserMapper;
import com.mbyte.easy.agent.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
@Service("ISysUserService")
public class SysUserServiceImpl extends ServiceImpl<AgentUserMapper, SysUser> implements ISysUserService {
    /**
     *
     */
    @Autowired
    AgentUserMapper agentUserMapper;
    @Override
    public int updateByUserAccount(SysUser agent){
        return agentUserMapper.updateByUserAccount(agent);
    }

    @Override
    public SysUser findByUserAccount(String account){
        return agentUserMapper.findByUserAccount(account);
    }
}
