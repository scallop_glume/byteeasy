package com.mbyte.easy.message.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.message.entity.Message;
import com.mbyte.easy.message.service.IMessageService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.operation.entity.Operation;
import com.mbyte.easy.operation.service.IOperationService;
import com.mbyte.easy.util.PageInfo;
import com.mbyte.easy.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.time.LocalDateTime;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@Controller
@RequestMapping("/message/message")
public class MessageController extends BaseController  {

    private String prefix = "message/message/";

    @Autowired
    private IMessageService messageService;
    @Autowired
    private IOperationService operationService;

    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param message
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, Message message) {
        Page<Message> page = new Page<Message>(pageNo, pageSize);
        QueryWrapper<Message> queryWrapper = new QueryWrapper<Message>();
        if(!ObjectUtils.isEmpty(message.getFromWho())) {
            queryWrapper = queryWrapper.like("fromWho",message.getFromWho());
        }
//        if(!ObjectUtils.isEmpty(message.getToWho())) {
            queryWrapper = queryWrapper.like("toWho",Utility.getCurrentUsername());
//        }
        if(!ObjectUtils.isEmpty(message.getMessage())) {
            queryWrapper = queryWrapper.like("message",message.getMessage());
        }
        if(!ObjectUtils.isEmpty(message.getStatus())) {
            queryWrapper = queryWrapper.like("status",message.getStatus());
        }
        IPage<Message> pageInfo = messageService.page(page, queryWrapper);
        model.addAttribute("searchInfo", message);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix+"list";
    }

    /**
     * 查询列表
     *
     * @param model
     * @param pageNo
     * @param pageSize
     * @param message
     * @return
     */
    @RequestMapping("me")
    public String index1(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, Message message) {
        Page<Message> page = new Page<Message>(pageNo, pageSize);
        QueryWrapper<Message> queryWrapper = new QueryWrapper<Message>();
//        if(!ObjectUtils.isEmpty(message.getFromWho())) {
            queryWrapper = queryWrapper.like("fromWho",Utility.getCurrentUsername());
//        }.

        if(!ObjectUtils.isEmpty(message.getToWho())) {
        queryWrapper = queryWrapper.like("toWho",message.getToWho());
        }
        if(!ObjectUtils.isEmpty(message.getMessage())) {
            queryWrapper = queryWrapper.like("message",message.getMessage());
        }
        if(!ObjectUtils.isEmpty(message.getStatus())) {
            queryWrapper = queryWrapper.like("status",message.getStatus());
        }
        IPage<Message> pageInfo = messageService.page(page, queryWrapper);
        model.addAttribute("searchInfo", message);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix+"list1";
    }



    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }
    /**
    * 添加
    * @param message
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(Message message){
        message.setFromWho(Utility.getCurrentUsername());
        message.setStatus(Message.UNREAD);
        Operation operation=new Operation();
        operation.setAccount(Utility.getCurrentUsername());
        operation.setTime(LocalDateTime.now());
        operation.setOperation(Utility.getCurrentUsername()+"向"+message.getToWho()+"发送了"+message.getMessage());
        operationService.save(operation);
        return toAjax(messageService.save(message));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        model.addAttribute("message",messageService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param message
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(Message message){
        message.setStatus(Message.UNREAD);
        message.setFromWho(Utility.getCurrentUsername());
        Operation operation=new Operation();
        operation.setAccount(Utility.getCurrentUsername());
        operation.setTime(LocalDateTime.now());
        operation.setOperation(Utility.getCurrentUsername()+"向"+message.getToWho()+"回复了"+message.getMessage());
        operationService.save(operation);
        return toAjax(messageService.save(message));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        Message byId = messageService.getById(id);
        byId.setStatus(Message.READ);
        Operation operation=new Operation();
        operation.setAccount(Utility.getCurrentUsername());
        operation.setTime(LocalDateTime.now());
        operation.setOperation(Utility.getCurrentUsername()+"将"+byId.toString()+"标记为已读");
        operationService.save(operation);
        return toAjax(messageService.updateById(byId));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        Collection<Message> messages = messageService.listByIds(ids);
        Iterator<Message> iterator = messages.iterator();
        String op="";
        while (iterator.hasNext()){
            Message next = iterator.next();
            next.setStatus(Message.READ);
            op+=next.toString()+",";
        }
        Operation operation=new Operation();
        operation.setTime(LocalDateTime.now());
        operation.setAccount(Utility.getCurrentUsername());
        operation.setOperation(Utility.getCurrentUsername()+"将"+op+"标记未为已读");
        return toAjax(messageService.updateBatchById(messages));
    }

}

