package com.mbyte.easy.message.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mbyte.easy.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 聊天记录
 * </p>
 *
 * @author lp
 * @since 2020-02-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_message")
public class Message extends BaseEntity {

    private static final long serialVersionUID = 1L;
    public static final int UNREAD=0;
    public static final int READ=1;

    /**
     * 发送人
     */
    @TableField("fromWho")
    private String fromWho;

    /**
     * 接收人
     */
    @TableField("toWho")
    private String toWho;

    /**
     * 信息
     */
    @TableField("message")
    private String message;

    /**
     * 状态 0:未读 1:已读
     */
    @TableField("status")
    private Integer status;
    @TableField(exist = false)
    private String statusName;
    public String getStatusName() {
        if(status==null){
            status=0;
        }
        switch (status){
            case Message.UNREAD:
                statusName="未读";
                break;
            case Message.READ:
                statusName="已读";
                break;
            default:
                statusName="未知";
                break;
        }
        return statusName;
    }
}
