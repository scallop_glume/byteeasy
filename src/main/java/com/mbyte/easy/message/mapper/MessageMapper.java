package com.mbyte.easy.message.mapper;

import com.mbyte.easy.message.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 聊天记录 Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-02-10
 */
public interface MessageMapper extends BaseMapper<Message> {

}
