package com.mbyte.easy.message.service;

import com.mbyte.easy.message.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 聊天记录 服务类
 * </p>
 *
 * @author lp
 * @since 2020-02-10
 */
public interface IMessageService extends IService<Message> {

}
