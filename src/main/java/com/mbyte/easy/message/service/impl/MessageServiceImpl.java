package com.mbyte.easy.message.service.impl;

import com.mbyte.easy.message.entity.Message;
import com.mbyte.easy.message.mapper.MessageMapper;
import com.mbyte.easy.message.service.IMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 聊天记录 服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-02-10
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements IMessageService {

}
