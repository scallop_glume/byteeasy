package com.mbyte.easy.myUser.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.myUser.entity.MyUser;
import com.mbyte.easy.myUser.service.IMyUserService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.operation.entity.Operation;
import com.mbyte.easy.operation.service.IOperationService;
import com.mbyte.easy.ticket.entity.Ticket;
import com.mbyte.easy.ticket.service.ITicketService;
import com.mbyte.easy.util.PageInfo;
import com.mbyte.easy.util.Utility;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@Controller
@RequestMapping("/myUser/myUser")
public class MyUserController extends BaseController  {

    private String prefix = "myUser/myUser/";

    @Autowired
    private IMyUserService myUserService;

    @Autowired
    private ITicketService ticketService;

    @Autowired
    private IOperationService operationService;
    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param myUser
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, MyUser myUser) {
        Page<MyUser> page = new Page<MyUser>(pageNo, pageSize);
        QueryWrapper<MyUser> queryWrapper = new QueryWrapper<MyUser>();
        if(!ObjectUtils.isEmpty(myUser.getName())) {
            queryWrapper = queryWrapper.like("name",myUser.getName());
         }
        if(!ObjectUtils.isEmpty(myUser.getAccount())) {
            queryWrapper = queryWrapper.like("account",myUser.getAccount());
         }
        if(!ObjectUtils.isEmpty(myUser.getPassword())) {
            queryWrapper = queryWrapper.like("password",myUser.getPassword());
         }
        if(!ObjectUtils.isEmpty(myUser.getVip())) {
            queryWrapper = queryWrapper.like("vip",myUser.getVip());
         }
        if(!ObjectUtils.isEmpty(myUser.getMoney())) {
            queryWrapper = queryWrapper.like("money",myUser.getMoney());
         }
        IPage<MyUser> pageInfo = myUserService.page(page, queryWrapper);
        model.addAttribute("searchInfo", myUser);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix+"list";
    }

    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }
    /**
    * 添加
    * @param myUser
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(MyUser myUser){
        return toAjax(myUserService.save(myUser));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        model.addAttribute("myUser",myUserService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param myUser
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(MyUser myUser){
        return toAjax(myUserService.updateById(myUser));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(myUserService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(myUserService.removeByIds(ids));
    }

    @RequestMapping("usebefore")
    public String usebefore(){
        return prefix+"use";
    }

    /**
     * 核销
     * @param ticket
     * @return
     */
    @PostMapping("usecard")
    @ResponseBody
    public AjaxResult usecard(Ticket ticket){
        ticket = ticketService.selectByAP(ticket);
        if(ticket.getStatus()==Ticket.CLOSURE){
            return toAjax(0);
        }
        ticket.setDeleteMan(Utility.getCurrentUsername());
        ticket.setDeleteDate(LocalDateTime.now());
        ticket.setStatus(Ticket.CLOSURE);
        System.out.println("--------------------------------------------------------"+ticket);

        String currentUsername = Utility.getCurrentUsername();
        Operation operation=new Operation();
        operation.setOperation("使用卡卷："+ticket.getTicketName()+ticket.getTicketNO());
        operation.setAccount(currentUsername);
        operation.setTime(LocalDateTime.now());
        operationService.save(operation);
        return toAjax(ticketService.updateById(ticket));
    }
}

