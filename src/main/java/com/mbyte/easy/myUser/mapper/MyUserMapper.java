package com.mbyte.easy.myUser.mapper;

import com.mbyte.easy.myUser.entity.MyUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
public interface MyUserMapper extends BaseMapper<MyUser> {

}
