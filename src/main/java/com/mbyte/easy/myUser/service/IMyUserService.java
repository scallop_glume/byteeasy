package com.mbyte.easy.myUser.service;

import com.mbyte.easy.myUser.entity.MyUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
public interface IMyUserService extends IService<MyUser> {

}
