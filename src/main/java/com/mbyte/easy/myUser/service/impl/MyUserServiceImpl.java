package com.mbyte.easy.myUser.service.impl;

import com.mbyte.easy.myUser.entity.MyUser;
import com.mbyte.easy.myUser.mapper.MyUserMapper;
import com.mbyte.easy.myUser.service.IMyUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
@Service
public class MyUserServiceImpl extends ServiceImpl<MyUserMapper, MyUser> implements IMyUserService {

}
