package com.mbyte.easy.operation.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.operation.entity.Operation;
import com.mbyte.easy.operation.service.IOperationService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@Controller
@RequestMapping("/operation/operation")
public class OperationController extends BaseController  {

    private String prefix = "operation/operation/";

    @Autowired
    private IOperationService operationService;

    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param operation
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String timeSpace, Operation operation) {
        Page<Operation> page = new Page<Operation>(pageNo, pageSize);
        QueryWrapper<Operation> queryWrapper = new QueryWrapper<Operation>();
        if(!ObjectUtils.isEmpty(operation.getAccount())) {
            queryWrapper = queryWrapper.like("account",operation.getAccount());
         }
        if(!ObjectUtils.isEmpty(operation.getOperation())) {
            queryWrapper = queryWrapper.like("operation",operation.getOperation());
         }
        if(!ObjectUtils.isEmpty(operation.getTime())) {
            queryWrapper = queryWrapper.like("time",operation.getTime());
         }
        IPage<Operation> pageInfo = operationService.page(page, queryWrapper);
        model.addAttribute("timeSpace", timeSpace);
        model.addAttribute("searchInfo", operation);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix+"list";
    }

    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }
    /**
    * 添加
    * @param operation
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(Operation operation){
        return toAjax(operationService.save(operation));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        model.addAttribute("operation",operationService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param operation
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(Operation operation){
        return toAjax(operationService.updateById(operation));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(operationService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(operationService.removeByIds(ids));
    }

}

