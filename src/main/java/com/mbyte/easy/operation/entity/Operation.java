package com.mbyte.easy.operation.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mbyte.easy.common.entity.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_operation")
public class Operation extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 账户
     */
    private String account;

    /**
     * 操作
     */
    private String operation;

    /**
     * 操作时间
     */
    private LocalDateTime time;


}
