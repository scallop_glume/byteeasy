package com.mbyte.easy.operation.mapper;

import com.mbyte.easy.operation.entity.Operation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
public interface OperationMapper extends BaseMapper<Operation> {

}
