package com.mbyte.easy.operation.service;

import com.mbyte.easy.operation.entity.Operation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
public interface IOperationService extends IService<Operation> {

}
