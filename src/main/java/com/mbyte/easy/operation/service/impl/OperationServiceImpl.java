package com.mbyte.easy.operation.service.impl;

import com.mbyte.easy.operation.entity.Operation;
import com.mbyte.easy.operation.mapper.OperationMapper;
import com.mbyte.easy.operation.service.IOperationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
@Service
public class OperationServiceImpl extends ServiceImpl<OperationMapper, Operation> implements IOperationService {

}
