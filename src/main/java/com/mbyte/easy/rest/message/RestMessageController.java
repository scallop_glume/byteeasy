package com.mbyte.easy.rest.message;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.message.entity.Message;
import com.mbyte.easy.message.service.IMessageService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@RestController
@RequestMapping("rest/message")
public class RestMessageController extends BaseController  {

    @Autowired
    private IMessageService messageService;

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @param message
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, Message message) {
        Page<Message> page = new Page<Message>(pageNo, pageSize);
        QueryWrapper<Message> queryWrapper = new QueryWrapper<Message>();

        if(message.getFromWho() != null  && !"".equals(message.getFromWho() + "")) {
            queryWrapper = queryWrapper.like("fromWho",message.getFromWho());
         }


        if(message.getToWho() != null  && !"".equals(message.getToWho() + "")) {
            queryWrapper = queryWrapper.like("toWho",message.getToWho());
         }


        if(message.getMessage() != null  && !"".equals(message.getMessage() + "")) {
            queryWrapper = queryWrapper.like("message",message.getMessage());
         }


        if(message.getStatus() != null  && !"".equals(message.getStatus() + "")) {
            queryWrapper = queryWrapper.like("status",message.getStatus());
         }

        IPage<Message> pageInfo = messageService.page(page, queryWrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("searchInfo",  message);
        map.put("pageInfo", new PageInfo(pageInfo));

        return this.success(map);
    }


    /**
    * 添加
    * @param message
    * @return
    */
    @PostMapping("add")
    public AjaxResult add(Message message){
        return toAjax(messageService.save(message));
    }

    /**
    * 添加
    * @param message
    * @return
    */
    @PostMapping("edit")
    public AjaxResult edit(Message message){
        return toAjax(messageService.updateById(message));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(messageService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(messageService.removeByIds(ids));
    }

}

