package com.mbyte.easy.rest.myUser;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.myUser.entity.MyUser;
import com.mbyte.easy.myUser.service.IMyUserService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@RestController
@RequestMapping("rest/myUser")
public class RestMyUserController extends BaseController  {

    @Autowired
    private IMyUserService myUserService;

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @param myUser
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, MyUser myUser) {
        Page<MyUser> page = new Page<MyUser>(pageNo, pageSize);
        QueryWrapper<MyUser> queryWrapper = new QueryWrapper<MyUser>();

        if(myUser.getName() != null  && !"".equals(myUser.getName() + "")) {
            queryWrapper = queryWrapper.like("name",myUser.getName());
         }


        if(myUser.getAccount() != null  && !"".equals(myUser.getAccount() + "")) {
            queryWrapper = queryWrapper.like("account",myUser.getAccount());
         }


        if(myUser.getPassword() != null  && !"".equals(myUser.getPassword() + "")) {
            queryWrapper = queryWrapper.like("password",myUser.getPassword());
         }


        if(myUser.getVip() != null  && !"".equals(myUser.getVip() + "")) {
            queryWrapper = queryWrapper.like("vip",myUser.getVip());
         }


        if(myUser.getMoney() != null  && !"".equals(myUser.getMoney() + "")) {
            queryWrapper = queryWrapper.like("money",myUser.getMoney());
         }

        IPage<MyUser> pageInfo = myUserService.page(page, queryWrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("searchInfo",  myUser);
        map.put("pageInfo", new PageInfo(pageInfo));

        return this.success(map);
    }


    /**
    * 添加
    * @param myUser
    * @return
    */
    @PostMapping("add")
    public AjaxResult add(MyUser myUser){
        return toAjax(myUserService.save(myUser));
    }

    /**
    * 添加
    * @param myUser
    * @return
    */
    @PostMapping("edit")
    public AjaxResult edit(MyUser myUser){
        return toAjax(myUserService.updateById(myUser));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(myUserService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(myUserService.removeByIds(ids));
    }

}

