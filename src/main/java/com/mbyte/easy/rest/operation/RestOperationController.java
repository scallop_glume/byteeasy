package com.mbyte.easy.rest.operation;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.operation.entity.Operation;
import com.mbyte.easy.operation.service.IOperationService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@RestController
@RequestMapping("rest/operation")
public class RestOperationController extends BaseController  {

    @Autowired
    private IOperationService operationService;

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @param operation
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String timeSpace, Operation operation) {
        Page<Operation> page = new Page<Operation>(pageNo, pageSize);
        QueryWrapper<Operation> queryWrapper = new QueryWrapper<Operation>();

        if(operation.getAccount() != null  && !"".equals(operation.getAccount() + "")) {
            queryWrapper = queryWrapper.like("account",operation.getAccount());
         }


        if(operation.getOperation() != null  && !"".equals(operation.getOperation() + "")) {
            queryWrapper = queryWrapper.like("operation",operation.getOperation());
         }


        if(operation.getTime() != null  && !"".equals(operation.getTime() + "")) {
            queryWrapper = queryWrapper.like("time",operation.getTime());
         }

        IPage<Operation> pageInfo = operationService.page(page, queryWrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("searchInfo",  operation);
        map.put("pageInfo", new PageInfo(pageInfo));

        return this.success(map);
    }


    /**
    * 添加
    * @param operation
    * @return
    */
    @PostMapping("add")
    public AjaxResult add(Operation operation){
        return toAjax(operationService.save(operation));
    }

    /**
    * 添加
    * @param operation
    * @return
    */
    @PostMapping("edit")
    public AjaxResult edit(Operation operation){
        return toAjax(operationService.updateById(operation));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(operationService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(operationService.removeByIds(ids));
    }

}

