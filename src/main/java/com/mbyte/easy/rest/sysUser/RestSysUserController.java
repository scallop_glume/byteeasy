package com.mbyte.easy.rest.sysUser;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.agent.entity.SysUser;
import com.mbyte.easy.agent.service.ISysUserService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@RestController
@RequestMapping("rest/sysUser")
public class RestSysUserController extends BaseController  {

    @Autowired
    private ISysUserService sysUserService;

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @param sysUser
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String createtimeSpace, String updatetimeSpace, SysUser sysUser) {
        Page<SysUser> page = new Page<SysUser>(pageNo, pageSize);
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();

        if(sysUser.getCreatetime() != null  && !"".equals(sysUser.getCreatetime() + "")) {
            queryWrapper = queryWrapper.like("createtime",sysUser.getCreatetime());
         }


        if(sysUser.getPassword() != null  && !"".equals(sysUser.getPassword() + "")) {
            queryWrapper = queryWrapper.like("password",sysUser.getPassword());
         }


        if(sysUser.getUpdatetime() != null  && !"".equals(sysUser.getUpdatetime() + "")) {
            queryWrapper = queryWrapper.like("updatetime",sysUser.getUpdatetime());
         }


        if(sysUser.getUsername() != null  && !"".equals(sysUser.getUsername() + "")) {
            queryWrapper = queryWrapper.like("username",sysUser.getUsername());
         }


        if(sysUser.getAvailable() != null  && !"".equals(sysUser.getAvailable() + "")) {
            queryWrapper = queryWrapper.like("available",sysUser.getAvailable());
         }


        if(sysUser.getEmail() != null  && !"".equals(sysUser.getEmail() + "")) {
            queryWrapper = queryWrapper.like("email",sysUser.getEmail());
         }


        if(sysUser.getTel() != null  && !"".equals(sysUser.getTel() + "")) {
            queryWrapper = queryWrapper.like("tel",sysUser.getTel());
         }


        if(sysUser.getSexType() != null  && !"".equals(sysUser.getSexType() + "")) {
            queryWrapper = queryWrapper.like("sex_type",sysUser.getSexType());
         }


        if(sysUser.getSalary() != null  && !"".equals(sysUser.getSalary() + "")) {
            queryWrapper = queryWrapper.like("salary",sysUser.getSalary());
         }


        if(sysUser.getVipNum() != null  && !"".equals(sysUser.getVipNum() + "")) {
            queryWrapper = queryWrapper.like("vipNum",sysUser.getVipNum());
         }

        IPage<SysUser> pageInfo = sysUserService.page(page, queryWrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("searchInfo",  sysUser);
        map.put("pageInfo", new PageInfo(pageInfo));

        return this.success(map);
    }


    /**
    * 添加
    * @param sysUser
    * @return
    */
    @PostMapping("add")
    public AjaxResult add(SysUser sysUser){
        return toAjax(sysUserService.save(sysUser));
    }

    /**
    * 添加
    * @param sysUser
    * @return
    */
    @PostMapping("edit")
    public AjaxResult edit(SysUser sysUser){
        return toAjax(sysUserService.updateById(sysUser));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(sysUserService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(sysUserService.removeByIds(ids));
    }

}

