package com.mbyte.easy.rest.ticket;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.ticket.entity.Ticket;
import com.mbyte.easy.ticket.service.ITicketService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@RestController
@RequestMapping("rest/ticket")
public class RestTicketController extends BaseController  {

    @Autowired
    private ITicketService ticketService;

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @param ticket
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String addDateSpace, String deleteDateSpace, Ticket ticket) {
        Page<Ticket> page = new Page<Ticket>(pageNo, pageSize);
        QueryWrapper<Ticket> queryWrapper = new QueryWrapper<Ticket>();

        if(ticket.getTicketId() != null  && !"".equals(ticket.getTicketId() + "")) {
            queryWrapper = queryWrapper.like("ticketId",ticket.getTicketId());
         }


        if(ticket.getTicketName() != null  && !"".equals(ticket.getTicketName() + "")) {
            queryWrapper = queryWrapper.like("ticketName",ticket.getTicketName());
         }


        if(ticket.getTicketNO() != null  && !"".equals(ticket.getTicketNO() + "")) {
            queryWrapper = queryWrapper.like("ticketNO",ticket.getTicketNO());
         }


        if(ticket.getTicketPwd() != null  && !"".equals(ticket.getTicketPwd() + "")) {
            queryWrapper = queryWrapper.like("ticketPwd",ticket.getTicketPwd());
         }


        if(ticket.getAddMan() != null  && !"".equals(ticket.getAddMan() + "")) {
            queryWrapper = queryWrapper.like("addMan",ticket.getAddMan());
         }


        if(ticket.getAddDate() != null  && !"".equals(ticket.getAddDate() + "")) {
            queryWrapper = queryWrapper.like("addDate",ticket.getAddDate());
         }


        if(ticket.getDeleteMan() != null  && !"".equals(ticket.getDeleteMan() + "")) {
            queryWrapper = queryWrapper.like("deleteMan",ticket.getDeleteMan());
         }


        if(ticket.getDeleteDate() != null  && !"".equals(ticket.getDeleteDate() + "")) {
            queryWrapper = queryWrapper.like("deleteDate",ticket.getDeleteDate());
         }


        if(ticket.getStatus() != null  && !"".equals(ticket.getStatus() + "")) {
            queryWrapper = queryWrapper.like("status",ticket.getStatus());
         }

        IPage<Ticket> pageInfo = ticketService.page(page, queryWrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("searchInfo",  ticket);
        map.put("pageInfo", new PageInfo(pageInfo));

        return this.success(map);
    }


    /**
    * 添加
    * @param ticket
    * @return
    */
    @PostMapping("add")
    public AjaxResult add(Ticket ticket){
        return toAjax(ticketService.save(ticket));
    }

    /**
    * 添加
    * @param ticket
    * @return
    */
    @PostMapping("edit")
    public AjaxResult edit(Ticket ticket){
        return toAjax(ticketService.updateById(ticket));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(ticketService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(ticketService.removeByIds(ids));
    }

}

