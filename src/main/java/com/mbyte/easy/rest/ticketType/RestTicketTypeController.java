package com.mbyte.easy.rest.ticketType;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.ticketType.entity.TicketType;
import com.mbyte.easy.ticketType.service.ITicketTypeService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@RestController
@RequestMapping("rest/ticketType")
public class RestTicketTypeController extends BaseController  {

    @Autowired
    private ITicketTypeService ticketTypeService;

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @param ticketType
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String addDateSpace, String publishDateSpace, String deleteDateSpace, TicketType ticketType) {
        Page<TicketType> page = new Page<TicketType>(pageNo, pageSize);
        QueryWrapper<TicketType> queryWrapper = new QueryWrapper<TicketType>();

        if(ticketType.getTicketName() != null  && !"".equals(ticketType.getTicketName() + "")) {
            queryWrapper = queryWrapper.like("ticketName",ticketType.getTicketName());
         }


        if(ticketType.getTicketContent() != null  && !"".equals(ticketType.getTicketContent() + "")) {
            queryWrapper = queryWrapper.like("ticketContent",ticketType.getTicketContent());
         }


        if(ticketType.getTicketStatus() != null  && !"".equals(ticketType.getTicketStatus() + "")) {
            queryWrapper = queryWrapper.like("ticketStatus",ticketType.getTicketStatus());
         }


        if(ticketType.getAddMan() != null  && !"".equals(ticketType.getAddMan() + "")) {
            queryWrapper = queryWrapper.like("addMan",ticketType.getAddMan());
         }


        if(ticketType.getPublishMan() != null  && !"".equals(ticketType.getPublishMan() + "")) {
            queryWrapper = queryWrapper.like("publishMan",ticketType.getPublishMan());
         }


        if(ticketType.getDeleteMan() != null  && !"".equals(ticketType.getDeleteMan() + "")) {
            queryWrapper = queryWrapper.like("deleteMan",ticketType.getDeleteMan());
         }


        if(ticketType.getAddDate() != null  && !"".equals(ticketType.getAddDate() + "")) {
            queryWrapper = queryWrapper.like("addDate",ticketType.getAddDate());
         }


        if(ticketType.getPublishDate() != null  && !"".equals(ticketType.getPublishDate() + "")) {
            queryWrapper = queryWrapper.like("publishDate",ticketType.getPublishDate());
         }


        if(ticketType.getDeleteDate() != null  && !"".equals(ticketType.getDeleteDate() + "")) {
            queryWrapper = queryWrapper.like("deleteDate",ticketType.getDeleteDate());
         }

        IPage<TicketType> pageInfo = ticketTypeService.page(page, queryWrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("searchInfo",  ticketType);
        map.put("pageInfo", new PageInfo(pageInfo));

        return this.success(map);
    }


    /**
    * 添加
    * @param ticketType
    * @return
    */
    @PostMapping("add")
    public AjaxResult add(TicketType ticketType){
        return toAjax(ticketTypeService.save(ticketType));
    }

    /**
    * 添加
    * @param ticketType
    * @return
    */
    @PostMapping("edit")
    public AjaxResult edit(TicketType ticketType){
        return toAjax(ticketTypeService.updateById(ticketType));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(ticketTypeService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(ticketTypeService.removeByIds(ids));
    }

}

