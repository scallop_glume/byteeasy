package com.mbyte.easy.sellticket.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.mbyte.easy.agent.service.ISysUserService;
import com.mbyte.easy.agent.entity.SysUser;
import com.mbyte.easy.operation.entity.Operation;
import com.mbyte.easy.operation.service.IOperationService;
import com.mbyte.easy.sellticket.entity.Ticket;
import com.mbyte.easy.sellticket.service.ISellTicketService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import com.mbyte.easy.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@Controller
@RequestMapping("/sellticket/ticket")
public class SellTicketController extends BaseController  {

    private String prefix = "sellticket/ticket/";

    @Autowired
    private ISellTicketService ticketService;
    @Autowired
    private ISysUserService agentService;

    @Autowired
    private IOperationService operationService;
    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param ticket
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String addDateSpace, String deleteDateSpace, Ticket ticket) {
        Page<Ticket> page = new Page<Ticket>(pageNo, pageSize);
        QueryWrapper<Ticket> queryWrapper = new QueryWrapper<Ticket>();
        if(!ObjectUtils.isEmpty(ticket.getTicketId())) {
            queryWrapper = queryWrapper.like("ticketId",ticket.getTicketId());
         }
        if(!ObjectUtils.isEmpty(ticket.getTicketName())) {
            queryWrapper = queryWrapper.like("ticketName",ticket.getTicketName());
         }
        if(!ObjectUtils.isEmpty(ticket.getTicketNO())) {
            queryWrapper = queryWrapper.like("ticketNO",ticket.getTicketNO());
         }
        if(!ObjectUtils.isEmpty(ticket.getTicketPwd())) {
            queryWrapper = queryWrapper.like("ticketPwd",ticket.getTicketPwd());
         }
        if(!ObjectUtils.isEmpty(ticket.getAddMan())) {
            queryWrapper = queryWrapper.like("addMan",ticket.getAddMan());
         }
        if(!ObjectUtils.isEmpty(ticket.getAddDate())) {
            queryWrapper = queryWrapper.like("addDate",ticket.getAddDate());
         }
        if(!ObjectUtils.isEmpty(ticket.getDeleteMan())) {
            queryWrapper = queryWrapper.like("deleteMan",ticket.getDeleteMan());
         }
        if(!ObjectUtils.isEmpty(ticket.getDeleteDate())) {
            queryWrapper = queryWrapper.like("deleteDate",ticket.getDeleteDate());
         }

            queryWrapper = queryWrapper.like("status",Ticket.FORSALE);

        IPage<Ticket> pageInfo = ticketService.page(page, queryWrapper);
        model.addAttribute("addDateSpace", addDateSpace);
        model.addAttribute("deleteDateSpace", deleteDateSpace);
        model.addAttribute("searchInfo", ticket);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        String currentUsername = Utility.getCurrentUsername();
        Operation operation=new Operation();
        operation.setOperation("查看卡卷：" );
        operation.setAccount(currentUsername);
        operation.setTime(LocalDateTime.now());
        operationService.save(operation);
        return prefix+"list";
    }

    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }
    /**
    * 添加
    * @param ticket
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(Ticket ticket){
        return toAjax(ticketService.save(ticket));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        model.addAttribute("ticket",ticketService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param ticket
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(Ticket ticket){
        return toAjax(ticketService.updateById(ticket));
    }

    /**
    * 卖卡
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        Ticket byId = ticketService.getById(id);
        byId.setStatus(Ticket.SALE);
        System.out.println(byId.getStatus());
        /**
         * @author LR
         * @date 2020年2月9日22:46:58
         * 给代理人加钱
         */
        //代理人账号
        String agentAccount = Utility.getCurrentUsername();
        SysUser agent=new SysUser();
        //填入代理人账户给此代理人加钱
        SysUser a=agentService.findByUserAccount(agentAccount);
        int salary=a.getSalary();
        salary+=10;
        agent.setSalary(salary);
        agent.setUsername(agentAccount);
        System.out.println("我在这里哈哈哈哈"+agent.getUsername()+""+agent.getSalary()+"结束了呀哈哈哈哈");

        agentService.updateByUserAccount(agent);

        Operation operation=new Operation();
        operation.setOperation("卖了一张卡" +byId.getTicketName()+byId.getTicketNO());
        operation.setAccount(agentAccount);
        operation.setTime(LocalDateTime.now());
        operationService.save(operation);
        return toAjax(ticketService.updateById(byId));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(ticketService.removeByIds(ids));
    }

}

