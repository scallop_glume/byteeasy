package com.mbyte.easy.sellticket.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mbyte.easy.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 卡券表
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_ticket")
public class Ticket extends BaseEntity {

    private static final long serialVersionUID = 1L;
    public static final int FORSALE = 0;
    public static final int SALE = 1;
    public static final int CLOSURE = 2;

    /**
     * 卡券类别ID
     */
    @TableField("ticketId")
    private Integer ticketId;

    /**
     * 券名字
     */
    @TableField("ticketName")
    private String ticketName;

    /**
     * 卡券号
     */
    @TableField("ticketNO")
    private String ticketNO;

    /**
     * 卡密
     */
    @TableField("ticketPwd")
    private String ticketPwd;

    /**
     * 添加人
     */
    @TableField("addMan")
    private String addMan;

    /**
     * 添加日期
     */
    @TableField("addDate")
    private LocalDateTime addDate;

    /**
     * 删除人
     */
    @TableField("deleteMan")
    private String deleteMan;

    /**
     * 删除日期
     */
    @TableField("deleteDate")
    private LocalDateTime deleteDate;

    /**
     * 状态 0:代售 1：已卖出
     */
    @TableField("status")
    private Integer status;
    @TableField(exist = false)
    private String ticketStatusName;

    public String getTicketStatusName() {
        if(status==null){
            status=0;
        }
        switch(status){
            case Ticket.FORSALE:
                ticketStatusName="待售";
                break;
            case Ticket.SALE:
                ticketStatusName="已卖出";
                break;
            case Ticket.CLOSURE:
                ticketStatusName="已核销";
                break;
            default:
                ticketStatusName="未知";
                break;
        }
        return ticketStatusName;
    }


}
