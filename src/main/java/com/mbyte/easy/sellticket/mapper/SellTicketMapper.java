package com.mbyte.easy.sellticket.mapper;

import com.mbyte.easy.sellticket.entity.Ticket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 卡券表 Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
public interface SellTicketMapper extends BaseMapper<Ticket> {

}
