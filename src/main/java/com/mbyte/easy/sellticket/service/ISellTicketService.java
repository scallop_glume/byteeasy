package com.mbyte.easy.sellticket.service;

import com.mbyte.easy.sellticket.entity.Ticket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 卡券表 服务类
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
public interface ISellTicketService extends IService<Ticket> {

}
