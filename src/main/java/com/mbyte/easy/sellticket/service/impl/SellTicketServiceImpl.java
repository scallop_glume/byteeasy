package com.mbyte.easy.sellticket.service.impl;

import com.mbyte.easy.sellticket.entity.Ticket;
import com.mbyte.easy.sellticket.mapper.SellTicketMapper;
import com.mbyte.easy.sellticket.service.ISellTicketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 卡券表 服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
@Service
public class SellTicketServiceImpl extends ServiceImpl<SellTicketMapper, Ticket> implements ISellTicketService {

}
