package com.mbyte.easy.selltype.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.operation.entity.Operation;
import com.mbyte.easy.operation.service.IOperationService;
import com.mbyte.easy.selltype.entity.TicketType;
import com.mbyte.easy.selltype.service.ISellTicketTypeService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import com.mbyte.easy.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@Controller
@RequestMapping("/selltype/ticketType")
public class SellTypeController extends BaseController  {

    private String prefix = "selltype/ticketType/";

    @Autowired
    private ISellTicketTypeService ticketTypeService;
    @Autowired
    private IOperationService operationService;
    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param ticketType
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String addDateSpace, String publishDateSpace, String deleteDateSpace, TicketType ticketType) {
        Page<TicketType> page = new Page<TicketType>(pageNo, pageSize);
        QueryWrapper<TicketType> queryWrapper = new QueryWrapper<TicketType>();
        if(!ObjectUtils.isEmpty(ticketType.getTicketName())) {
            queryWrapper = queryWrapper.like("ticketName",ticketType.getTicketName());
         }
        if(!ObjectUtils.isEmpty(ticketType.getTicketContent())) {
            queryWrapper = queryWrapper.like("ticketContent",ticketType.getTicketContent());
         }
        if(!ObjectUtils.isEmpty(ticketType.getTicketStatus())) {
            queryWrapper = queryWrapper.like("ticketStatus",ticketType.getTicketStatus());
         }
        if(!ObjectUtils.isEmpty(ticketType.getAddMan())) {
            queryWrapper = queryWrapper.like("addMan",ticketType.getAddMan());
         }
        if(!ObjectUtils.isEmpty(ticketType.getPublishMan())) {
            queryWrapper = queryWrapper.like("publishMan",ticketType.getPublishMan());
         }
        if(!ObjectUtils.isEmpty(ticketType.getDeleteMan())) {
            queryWrapper = queryWrapper.like("deleteMan",ticketType.getDeleteMan());
         }
        if(!ObjectUtils.isEmpty(ticketType.getAddDate())) {
            queryWrapper = queryWrapper.like("addDate",ticketType.getAddDate());
         }
        if(!ObjectUtils.isEmpty(ticketType.getPublishDate())) {
            queryWrapper = queryWrapper.like("publishDate",ticketType.getPublishDate());
         }
        if(!ObjectUtils.isEmpty(ticketType.getDeleteDate())) {
            queryWrapper = queryWrapper.like("deleteDate",ticketType.getDeleteDate());
         }
        IPage<TicketType> pageInfo = ticketTypeService.page(page, queryWrapper);
        model.addAttribute("addDateSpace", addDateSpace);
        model.addAttribute("publishDateSpace", publishDateSpace);
        model.addAttribute("deleteDateSpace", deleteDateSpace);
        model.addAttribute("searchInfo", ticketType);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        String currentUsername = Utility.getCurrentUsername();
        Operation operation=new Operation();
        operation.setOperation("查看卡卷总类别：" );
        operation.setAccount(currentUsername);
        operation.setTime(LocalDateTime.now());
        operationService.save(operation);
        return prefix+"list";
    }

    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }
    /**
    * 添加
    * @param ticketType
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(TicketType ticketType){
        return toAjax(ticketTypeService.save(ticketType));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        model.addAttribute("ticketType",ticketTypeService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param ticketType
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(TicketType ticketType){
        return toAjax(ticketTypeService.updateById(ticketType));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(ticketTypeService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(ticketTypeService.removeByIds(ids));
    }

}

