package com.mbyte.easy.selltype.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mbyte.easy.common.entity.BaseEntity;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 卡券类型表
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_ticket_type")
public class TicketType extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 卡券名
     */
    @TableField("ticketName")
    private String ticketName;

    /**
     * 卡券内容
     */
    @TableField("ticketContent")
    private String ticketContent;

    /**
     * 券状态
     */
    @TableField("ticketStatus")
    private Integer ticketStatus;

    /**
     * 添加人
     */
    @TableField("addMan")
    private String addMan;

    /**
     * 发布人
     */
    @TableField("publishMan")
    private String publishMan;

    /**
     * 删除人
     */
    @TableField("deleteMan")
    private String deleteMan;

    /**
     * 添加日期
     */
    @TableField("addDate")
    private LocalDateTime addDate;

    /**
     * 发布日期
     */
    @TableField("publishDate")
    private LocalDateTime publishDate;

    /**
     * 删除日期
     */
    @TableField("deleteDate")
    private LocalDateTime deleteDate;


}
