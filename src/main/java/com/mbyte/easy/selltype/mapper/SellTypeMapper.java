package com.mbyte.easy.selltype.mapper;

import com.mbyte.easy.selltype.entity.TicketType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 卡券类型表 Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
public interface SellTypeMapper extends BaseMapper<TicketType> {

}
