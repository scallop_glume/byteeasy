package com.mbyte.easy.selltype.service;

import com.mbyte.easy.selltype.entity.TicketType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 卡券类型表 服务类
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
public interface ISellTicketTypeService extends IService<TicketType> {

}
