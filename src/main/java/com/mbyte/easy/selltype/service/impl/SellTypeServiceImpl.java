package com.mbyte.easy.selltype.service.impl;

import com.mbyte.easy.selltype.entity.TicketType;
import com.mbyte.easy.selltype.mapper.SellTypeMapper;
import com.mbyte.easy.selltype.service.ISellTicketTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 卡券类型表 服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-02-09
 */
@Service
public class SellTypeServiceImpl extends ServiceImpl<SellTypeMapper, TicketType> implements ISellTicketTypeService {

}
