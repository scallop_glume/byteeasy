package com.mbyte.easy.ticket.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.ticket.entity.DicTicketStatus;
import com.mbyte.easy.ticket.entity.Ticket;
import com.mbyte.easy.ticket.service.ITicketService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.ticketType.entity.TicketType;
import com.mbyte.easy.util.PageInfo;
import com.mbyte.easy.util.Utility;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@Controller
@RequestMapping("/ticket/ticket")
public class TicketController extends BaseController  {

    private String prefix = "ticket/ticket/";

    @Autowired
    private ITicketService ticketService;

    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param ticket
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String addDateSpace, String deleteDateSpace, Ticket ticket) {
        Page<Ticket> page = new Page<Ticket>(pageNo, pageSize);
        QueryWrapper<Ticket> queryWrapper = new QueryWrapper<Ticket>();
        if(!ObjectUtils.isEmpty(ticket.getTicketId())) {
            queryWrapper = queryWrapper.like("ticketId",ticket.getTicketId());
         }
        if(!ObjectUtils.isEmpty(ticket.getTicketName())) {
            queryWrapper = queryWrapper.like("ticketName",ticket.getTicketName());
         }
        if(!ObjectUtils.isEmpty(ticket.getTicketNO())) {
            queryWrapper = queryWrapper.like("ticketNO",ticket.getTicketNO());
         }
        if(!ObjectUtils.isEmpty(ticket.getTicketPwd())) {
            queryWrapper = queryWrapper.like("ticketPwd",ticket.getTicketPwd());
         }
        if(!ObjectUtils.isEmpty(ticket.getAddMan())) {
            queryWrapper = queryWrapper.like("addMan",ticket.getAddMan());
         }
        if(!ObjectUtils.isEmpty(ticket.getAddDate())) {
            queryWrapper = queryWrapper.like("addDate",ticket.getAddDate());
         }
        if(!ObjectUtils.isEmpty(ticket.getDeleteMan())) {
            queryWrapper = queryWrapper.like("deleteMan",ticket.getDeleteMan());
         }
        if(!ObjectUtils.isEmpty(ticket.getDeleteDate())) {
            queryWrapper = queryWrapper.like("deleteDate",ticket.getDeleteDate());
         }
        if(!ObjectUtils.isEmpty(ticket.getStatus())) {
            queryWrapper = queryWrapper.like("status",ticket.getStatus());
         }
        IPage<Ticket> pageInfo = ticketService.page(page, queryWrapper);
        model.addAttribute("addDateSpace", addDateSpace);
        model.addAttribute("deleteDateSpace", deleteDateSpace);
        model.addAttribute("searchInfo", ticket);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        setDicTickStatus(model);
        return prefix+"list";
    }


    private void setDicTickStatus(Model model){
        Ticket ticket = new Ticket();
        List<DicTicketStatus> list=new ArrayList<>();
        DicTicketStatus forSale=new DicTicketStatus();
        forSale.setCode(Ticket.FORSALE);
        ticket.setStatus(Ticket.FORSALE);
        forSale.setMsg(ticket.getTicketStatusName());
        list.add(forSale);
        DicTicketStatus sale=new DicTicketStatus();
        sale.setCode(Ticket.SALE);
        ticket.setStatus(Ticket.SALE);
        sale.setMsg(ticket.getTicketStatusName());
        list.add(sale);
        DicTicketStatus closure=new DicTicketStatus();
        closure.setCode(Ticket.CLOSURE);
        ticket.setStatus(Ticket.CLOSURE);
        closure.setMsg(ticket.getTicketStatusName());
        list.add(closure);
        model.addAttribute("dicStatus",list);
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }
    /**
    * 添加
    * @param ticket
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(Ticket ticket){
        return toAjax(ticketService.save(ticket));
    }
    /**
    * 卖出卡券
    * @return
    */
    @GetMapping("editBefore/{id}")
    @ResponseBody
    public AjaxResult editBefore(@PathVariable("id")Long id){
        Ticket byId = ticketService.getById(id);
        byId.setStatus(Ticket.SALE);
        return toAjax(ticketService.updateById(byId));
    }
    /**
    * 添加
    * @param ticket
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(Ticket ticket){
        return toAjax(ticketService.updateById(ticket));
    }
    /**
    * 单个核销
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        Ticket byId = ticketService.getById(id);
        String currentUsername = Utility.getCurrentUsername();
        byId.setStatus(Ticket.CLOSURE);
        byId.setDeleteMan(currentUsername);
        byId.setDeleteDate(LocalDateTime.now());
        return toAjax(ticketService.updateById(byId));
    }
    /**
    * 批量核销
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        String currentUsername = Utility.getCurrentUsername();
        Collection<Ticket> tickets = ticketService.listByIds(ids);
        Iterator<Ticket> iterator = tickets.iterator();
        while(iterator.hasNext()){
            Ticket next = iterator.next();
            next.setDeleteMan(currentUsername);
            next.setDeleteDate(LocalDateTime.now());
            next.setStatus(Ticket.CLOSURE);
        }
        return toAjax(ticketService.updateBatchById(tickets));
    }

}

