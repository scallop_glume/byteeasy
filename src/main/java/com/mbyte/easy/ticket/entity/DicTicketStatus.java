package com.mbyte.easy.ticket.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class DicTicketStatus {
    private int code;
    private String msg;
}
