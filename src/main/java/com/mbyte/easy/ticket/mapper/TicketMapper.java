package com.mbyte.easy.ticket.mapper;

import com.mbyte.easy.ticket.entity.Ticket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 卡券表 Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
public interface TicketMapper extends BaseMapper<Ticket> {

     Ticket selectByAP(Ticket ticket);

}
