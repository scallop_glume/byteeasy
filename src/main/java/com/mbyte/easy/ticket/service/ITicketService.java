package com.mbyte.easy.ticket.service;

import com.mbyte.easy.ticket.entity.Ticket;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mbyte.easy.ticketType.entity.TicketType;

/**
 * <p>
 * 卡券表 服务类
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
public interface ITicketService extends IService<Ticket> {
    /**
     * 批量生成卡券
     * @param ticketTypeId 卡券类别ID
     * @param num 要生成的卡券数量
     * @return
     */
    boolean add(Long ticketTypeId,int num);


    Ticket selectByAP(Ticket ticket);
}
