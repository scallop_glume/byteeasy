package com.mbyte.easy.ticket.service.impl;

import com.mbyte.easy.ticket.entity.Ticket;
import com.mbyte.easy.ticket.mapper.TicketMapper;
import com.mbyte.easy.ticket.service.ITicketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mbyte.easy.ticketType.entity.TicketType;
import com.mbyte.easy.ticketType.mapper.TicketTypeMapper;
import com.mbyte.easy.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 卡券表 服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
@Service
public class TicketServiceImpl extends ServiceImpl<TicketMapper, Ticket> implements ITicketService {
    @Autowired
    private TicketTypeMapper ticketTypeMapper;
    @Autowired
    private TicketMapper ticketMapper;
    @Override
    public boolean add(Long ticketTypeId, int num) {
        TicketType ticketType = ticketTypeMapper.selectById(ticketTypeId);
        String currentUsername = Utility.getCurrentUsername();
        List<Ticket> list=new ArrayList<>(num);
        while (num>0){
            Ticket ticket=new Ticket();
            ticket.setTicketId(ticketTypeId.intValue());
            ticket.setAddMan(currentUsername);
            ticket.setAddDate(LocalDateTime.now());
            ticket.setTicketName(ticketType.getTicketName());
            ticket.setTicketNO(Utility.getRandomStrByNum(6));
            ticket.setTicketPwd(Utility.getRandomStrByNum(6));
            ticket.setStatus(Ticket.FORSALE);
            list.add(ticket);
            num--;
        }
        return saveBatch(list);
    }

    @Override

    public Ticket selectByAP(Ticket ticket) {
        return ticketMapper.selectByAP(ticket);
    }
}
