package com.mbyte.easy.ticketType.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.operation.entity.Operation;
import com.mbyte.easy.operation.service.IOperationService;
import com.mbyte.easy.ticket.entity.Ticket;
import com.mbyte.easy.ticket.service.ITicketService;
import com.mbyte.easy.ticketType.entity.DicTicketTypeStatus;
import com.mbyte.easy.ticketType.entity.TicketType;
import com.mbyte.easy.ticketType.service.ITicketTypeService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import com.mbyte.easy.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.time.LocalDateTime;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@Controller
@RequestMapping("/ticketType/ticketType")
public class TicketTypeController extends BaseController  {

    private String prefix = "ticketType/ticketType/";

    @Autowired
    private ITicketTypeService ticketTypeService;
    @Autowired
    private ITicketService ticketService;
    @Autowired
    private IOperationService operationService;

    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param ticketType
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String addDateSpace, String publishDateSpace, String deleteDateSpace, TicketType ticketType) {
        Page<TicketType> page = new Page<TicketType>(pageNo, pageSize);
        QueryWrapper<TicketType> queryWrapper = new QueryWrapper<TicketType>();
        if(!ObjectUtils.isEmpty(ticketType.getTicketName())) {
            queryWrapper = queryWrapper.like("ticketName",ticketType.getTicketName());
         }
        if(!ObjectUtils.isEmpty(ticketType.getTicketContent())) {
            queryWrapper = queryWrapper.like("ticketContent",ticketType.getTicketContent());
         }
        if(!ObjectUtils.isEmpty(ticketType.getTicketStatus())) {
            queryWrapper = queryWrapper.like("ticketStatus",ticketType.getTicketStatus());
         }
        if(!ObjectUtils.isEmpty(ticketType.getAddMan())) {
            queryWrapper = queryWrapper.like("addMan",ticketType.getAddMan());
         }
        if(!ObjectUtils.isEmpty(ticketType.getPublishMan())) {
            queryWrapper = queryWrapper.like("publishMan",ticketType.getPublishMan());
         }
        if(!ObjectUtils.isEmpty(ticketType.getDeleteMan())) {
            queryWrapper = queryWrapper.like("deleteMan",ticketType.getDeleteMan());
         }
        if(!ObjectUtils.isEmpty(ticketType.getAddDate())) {
            queryWrapper = queryWrapper.like("addDate",ticketType.getAddDate());
         }
        if(!ObjectUtils.isEmpty(ticketType.getPublishDate())) {
            queryWrapper = queryWrapper.like("publishDate",ticketType.getPublishDate());
         }
        if(!ObjectUtils.isEmpty(ticketType.getDeleteDate())) {
            queryWrapper = queryWrapper.like("deleteDate",ticketType.getDeleteDate());
         }
        IPage<TicketType> pageInfo = ticketTypeService.page(page, queryWrapper);
        model.addAttribute("addDateSpace", addDateSpace);
        model.addAttribute("publishDateSpace", publishDateSpace);
        model.addAttribute("deleteDateSpace", deleteDateSpace);
        model.addAttribute("searchInfo", ticketType);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        setdicTicketTypeStatus(model);
        return prefix+"list";
    }

    private void setdicTicketTypeStatus(Model model){
        List<DicTicketTypeStatus> list=new ArrayList<>();
        TicketType ticketType=new TicketType();
        DicTicketTypeStatus add=new DicTicketTypeStatus();
        ticketType.setTicketStatus(TicketType.ADD);
        add.setCode(TicketType.ADD);
        add.setMsg(ticketType.getTicketStatusName());
        list.add(add);
        DicTicketTypeStatus publish=new DicTicketTypeStatus();
        ticketType.setTicketStatus(TicketType.PUBLISH);
        publish.setCode(TicketType.PUBLISH);
        publish.setMsg(ticketType.getTicketStatusName());
        list.add(publish);
        DicTicketTypeStatus delete=new DicTicketTypeStatus();
        ticketType.setTicketStatus(TicketType.DELETE);
        delete.setCode(TicketType.DELETE);
        delete.setMsg(ticketType.getTicketStatusName());
        list.add(delete);
        model.addAttribute("dicStatus",list);
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }


    /**
    * 添加
    * @param ticketType
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(TicketType ticketType){
        String currentUsername = Utility.getCurrentUsername();
        ticketType.setAddMan(currentUsername);
        ticketType.setAddDate(new Timestamp(System.currentTimeMillis()));
        ticketType.setTicketStatus(TicketType.ADD);
        Operation operation=new Operation();
        operation.setOperation("增加卡券类别："+ticketType.getTicketName());
        operation.setAccount(currentUsername);
        operation.setTime(LocalDateTime.now());
        operationService.save(operation);
        return toAjax(ticketTypeService.save(ticketType));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        model.addAttribute("ticketType",ticketTypeService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param ticketType
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(TicketType ticketType){
        String currentUsername = Utility.getCurrentUsername();
        Operation operation=new Operation();
        operation.setTime(LocalDateTime.now());
        operation.setAccount(currentUsername);
        if(ticketType.getTicketStatus()==TicketType.PUBLISH){
            ticketType.setPublishMan(currentUsername);
            ticketType.setPublishDate(new Timestamp(System.currentTimeMillis()));
            operation.setOperation("将"+ticketType.getTicketName()+"类卡片状态由未发布改为已发布");
        }else {
            operation.setOperation("将"+ticketType.getTicketName()+"类卡片类别状态由已发布改为未发布");
        }
        operationService.save(operation);
        return toAjax(ticketTypeService.updateById(ticketType));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        TicketType byId = ticketTypeService.getById(id);
        String currentUsername = Utility.getCurrentUsername();
        byId.setDeleteDate(new Timestamp(System.currentTimeMillis()));
        byId.setDeleteMan(currentUsername);
        byId.setTicketStatus(TicketType.DELETE);
        Operation operation=new Operation();
        operation.setTime(LocalDateTime.now());
        operation.setAccount(currentUsername);
        operation.setOperation("删除"+byId.getTicketName()+"类卡片类别");
        operationService.save(operation);
        return toAjax(ticketTypeService.updateById(byId));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        Collection<TicketType> ticketTypes = ticketTypeService.listByIds(ids);
        Iterator<TicketType> iterator = ticketTypes.iterator();
        String currentUsername = Utility.getCurrentUsername();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String op="";
        while(iterator.hasNext()){
            TicketType next = iterator.next();
            next.setDeleteMan(currentUsername);
            next.setDeleteDate(timestamp);
            next.setTicketStatus(TicketType.DELETE);
            op+=next.getTicketName()+",";
        }
        Operation operation=new Operation();
        operation.setAccount(currentUsername);
        operation.setTime(LocalDateTime.now());
        operation.setOperation("批量删除卡券类别："+op);
        operationService.save(operation);
        return toAjax(ticketTypeService.updateBatchById(ticketTypes));
    }

    /**
     * 卡券生成跳转页面
     * @return
     */
    @GetMapping("addTicketBefore/{id}")
    public String addTicketBefore(Model model,@PathVariable("id")Long id) {
        model.addAttribute("ticketType",ticketTypeService.getById(id));
        return prefix+"addTicket";
    }

    /**
     * 批量添加卡券
     * @param id
     * @param num
     * @return
     */
    @PostMapping("addTicket")
    @ResponseBody
    public AjaxResult addTicket(Long id,Integer num){
        TicketType byId = ticketTypeService.getById(id);
        Operation operation=new Operation();
        String currentUsername = Utility.getCurrentUsername();
        operation.setTime(LocalDateTime.now());
        operation.setAccount(currentUsername);
        operation.setOperation("批量添加了卡券类别为"+byId.getTicketName()+"的卡券，生成了"+num+"张");
        operationService.save(operation);
        return toAjax(ticketService.add(id,num));
    }

}

