package com.mbyte.easy.ticketType.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class DicTicketTypeStatus {
    private int code;
    private String msg;
}
