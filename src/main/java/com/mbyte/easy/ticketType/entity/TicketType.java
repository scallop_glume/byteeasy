package com.mbyte.easy.ticketType.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mbyte.easy.common.entity.BaseEntity;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 卡券类型表
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_ticket_type")
public class TicketType extends BaseEntity {

    private static final long serialVersionUID = 1L;
    public static final int ADD = 0;
    public static final int PUBLISH = 1;
    public static final int DELETE = 2;

    /**
     * 卡券名
     */
    @TableField("ticketName")
    private String ticketName;

    /**
     * 卡券内容
     */
    @TableField("ticketContent")
    private String ticketContent;

    /**
     * 券状态 0:未发布 1:发布 2:删除
     */
    @TableField("ticketStatus")
    private Integer ticketStatus;

    @TableField(exist = false)
    private String ticketStatusName;

    public String getTicketStatusName() {
        if(ticketStatus==null){
            ticketStatus=0;
        }
        switch(ticketStatus){
            case TicketType.ADD:
                ticketStatusName="未发布";
                break;
            case TicketType.PUBLISH:
                ticketStatusName="发布";
                break;
            case TicketType.DELETE:
                ticketStatusName="已删除";
                break;
                default:
                    ticketStatusName="未知";
                    break;
        }
        return ticketStatusName;
    }

    /**
     * 添加人
     */
    @TableField("addMan")
    private String addMan;

    /**
     * 发布人
     */
    @TableField("publishMan")
    private String publishMan;

    /**
     * 删除人
     */
    @TableField("deleteMan")
    private String deleteMan;

    /**
     * 添加日期
     */
    @TableField("addDate")
    private Timestamp addDate;

    /**
     * 发布日期
     */
    @TableField("publishDate")
    private Timestamp publishDate;

    /**
     * 删除日期
     */
    @TableField("deleteDate")
    private Timestamp deleteDate;



}
