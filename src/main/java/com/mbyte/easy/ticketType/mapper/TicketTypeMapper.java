package com.mbyte.easy.ticketType.mapper;

import com.mbyte.easy.ticketType.entity.TicketType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 卡券类型表 Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
public interface TicketTypeMapper extends BaseMapper<TicketType> {

}
