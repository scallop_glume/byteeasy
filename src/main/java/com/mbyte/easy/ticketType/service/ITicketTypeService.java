package com.mbyte.easy.ticketType.service;

import com.mbyte.easy.ticketType.entity.TicketType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 卡券类型表 服务类
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
public interface ITicketTypeService extends IService<TicketType> {

}
