package com.mbyte.easy.ticketType.service.impl;

import com.mbyte.easy.ticketType.entity.TicketType;
import com.mbyte.easy.ticketType.mapper.TicketTypeMapper;
import com.mbyte.easy.ticketType.service.ITicketTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 卡券类型表 服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-02-08
 */
@Service
public class TicketTypeServiceImpl extends ServiceImpl<TicketTypeMapper, TicketType> implements ITicketTypeService {

}
